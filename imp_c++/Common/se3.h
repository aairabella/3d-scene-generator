// This file is part of REMODE - REgularized MOnocular Depth Estimation.
//
// Copyright (C) 2014 Matia Pizzoli <matia dot pizzoli at gmail dot com>
// Robotics and Perception Group, University of Zurich, Switzerland
// http://rpg.ifi.uzh.ch
//
// REMODE is free software: you can redistribute it and/or modify it under the
// terms of the GNU General Public License as published by the Free Software
// Foundation, either version 3 of the License, or any later version.
//
// REMODE is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
// FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

#ifndef RMD_SE3_H
#define RMD_SE3_H

#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>
//#include <opencv2/core/core.hpp>

template<typename Type>
struct SE3
{
  //__host__ __device__ __forceinline__
    SE3()
    {
        /* TODO: initialize to [eye(3) [0 0 0]']*/
        eye();
    }

  /// Constructor from a normalized quaternion and a translation vector
  inline SE3(Type qw, Type qx, Type qy, Type qz, Type tx, Type ty, Type tz)
  {
    const Type x  = 2*qx;
    const Type y  = 2*qy;
    const Type z  = 2*qz;
    const Type wx = x*qw;
    const Type wy = y*qw;
    const Type wz = z*qw;
    const Type xx = x*qx;
    const Type xy = y*qx;
    const Type xz = z*qx;
    const Type yy = y*qy;
    const Type yz = z*qy;
    const Type zz = z*qz;

    data(0, 0) = 1-(yy+zz);
    data(0, 1) = xy-wz;
    data(0, 2) = xz+wy;
    data(1, 0) = xy+wz;
    data(1, 1) = 1-(xx+zz);
    data(1, 2) = yz-wx;
    data(2, 0) = xz-wy;
    data(2, 1) = yz+wx;
    data(2, 2) = 1-(xx+yy);

    data(0, 3) = tx;
    data(1, 3) = ty;
    data(2, 3) = tz;
  }

  /// Construct from C arrays
  /// r is rotation matrix row major
  /// t is the translation vector (x y z)
  inline SE3(Type *r, Type *t)
  {
    data(0,0)=r[0]; data(0,1)=r[1]; data(0,2)=r[2]; data(0,3)=t[0];
    data(1,0)=r[3]; data(1,1)=r[4]; data(1,2)=r[5]; data(1,3)=t[1];
    data(2,0)=r[6]; data(2,1)=r[7]; data(2,2)=r[8]; data(2,3)=t[2];
  }

  inline SE3(cv::Mat_<float> r, cv::Point3f t)
  {
    data(0,0)=r(0,0); data(0,1)=r(0,1); data(0,2)=r(0,2); data(0,3)=t.x;
    data(1,0)=r(1,0); data(1,1)=r(1,1); data(1,2)=r(1,2); data(1,3)=t.y;
    data(2,0)=r(2,0); data(2,1)=r(2,1); data(2,2)=r(2,2); data(2,3)=t.z;
  }

  void eye(){
      data(0,0)=1.0; data(0,1)=0.0; data(0,2)=0.0; data(0,3)=0.0;
      data(1,0)=0.0; data(1,1)=1.0; data(1,2)=0.0; data(1,3)=0.0;
      data(2,0)=0.0; data(2,1)=0.0; data(2,2)=1.0; data(2,3)=0.0;
  }

  inline SE3<Type> inv() const
  {
    SE3<Type> result;
    result.data(0,0)  = data(0,0);
    result.data(0,1)  = data(1,0);
    result.data(0,2)  = data(2,0);
    result.data(1,0)  = data(0,1);
    result.data(1,1)  = data(1,1);
    result.data(1,2)  = data(2,1);
    result.data(2,0)  = data(0,2);
    result.data(2,1)  = data(1,2);
    result.data(2,2)  = data(2,2);
    result.data(0,3)  = -data(0,0)*data(0,3) -data(1,0)*data(1,3) -data(2,0)*data(2,3);
    result.data(1,3)  = -data(0,1)*data(0,3) -data(1,1)*data(1,3) -data(2,1)*data(2,3);
    result.data(2,3)  = -data(0,2)*data(0,3) -data(1,2)*data(1,3) -data(2,2)*data(2,3);

    return result;
  }

  inline Type operator()(int r, int c) const
  {
    return data(r, c);
  }

  inline Type & operator()(int r, int c)
  {
    return data(r, c);
  }

  inline cv::Point3_<Type> rotate(const cv::Point3_<Type> &p) const
  {
    return cv::Point3_<Type>(data(0,0)*p.x + data(0,1)*p.y + data(0,2)*p.z,
                       data(1,0)*p.x + data(1,1)*p.y + data(1,2)*p.z,
                       data(2,0)*p.x + data(2,1)*p.y + data(2,2)*p.z);
  }

  inline cv::Point3_<Type> translate(const cv::Point3_<Type> &p) const
  {
    return cv::Point3_<Type>(p.x + data(0,3),
                       p.y + data(1,3),
                       p.z + data(2,3));
  }

  inline cv::Point3_<Type> getTranslation() const
  {
    return cv::Point3_<Type>(data(0,3),
                       data(1,3),
                       data(2,3));
  }

  friend std::ostream & operator<<(std::ostream &out, const SE3 &m)
  {
    out << m.data;
    return out;
  }

  cv::Matx<Type, 3, 4> data;
};


template<typename Type>
inline SE3<Type> operator*(const SE3<Type> &lhs, const SE3<Type> &rhs)
{
  SE3<Type> result;
  result.data(0,0)  = lhs.data(0,0)*rhs.data(0,0) + lhs.data(0,1)*rhs.data(1,0) + lhs.data(0,2)*rhs.data(2,0);
  result.data(0,1)  = lhs.data(0,0)*rhs.data(0,1) + lhs.data(0,1)*rhs.data(1,1) + lhs.data(0,2)*rhs.data(2,1);
  result.data(0,2)  = lhs.data(0,0)*rhs.data(0,2) + lhs.data(0,1)*rhs.data(1,2) + lhs.data(0,2)*rhs.data(2,2);
  result.data(0,3)  = lhs.data(0,3) + lhs.data(0,0)*rhs.data(0,3) + lhs.data(0,1)*rhs.data(1,3) + lhs.data(0,2)*rhs.data(2,3);
  result.data(1,0)  = lhs.data(1,0)*rhs.data(0,0) + lhs.data(1,1)*rhs.data(1,0) + lhs.data(1,2)*rhs.data(2,0);
  result.data(1,1)  = lhs.data(1,0)*rhs.data(0,1) + lhs.data(1,1)*rhs.data(1,1) + lhs.data(1,2)*rhs.data(2,1);
  result.data(1,2)  = lhs.data(1,0)*rhs.data(0,2) + lhs.data(1,1)*rhs.data(1,2) + lhs.data(1,2)*rhs.data(2,2);
  result.data(1,3)  = lhs.data(1,3) + lhs.data(1,0)*rhs.data(0,3) + lhs.data(1,1)*rhs.data(1,3) + lhs.data(1,2)*rhs.data(2,3);
  result.data(2,0)  = lhs.data(2,0)*rhs.data(0,0) + lhs.data(2,1)*rhs.data(1,0) + lhs.data(2,2)*rhs.data(2,0);
  result.data(2,1)  = lhs.data(2,0)*rhs.data(0,1) + lhs.data(2,1)*rhs.data(1,1) + lhs.data(2,2)*rhs.data(2,1);
  result.data(2,2)  = lhs.data(2,0)*rhs.data(0,2) + lhs.data(2,1)*rhs.data(1,2) + lhs.data(2,2)*rhs.data(2,2);
  result.data(2,3)  = lhs.data(2,3) + lhs.data(2,0)*rhs.data(0,3) + lhs.data(2,1)*rhs.data(1,3) + lhs.data(2,2)*rhs.data(2,3);

  return result;
}

template<typename Type>
inline cv::Point3_<Type> operator*(const SE3<Type> &se3, const cv::Point3_<Type> &p)
{
  return se3.translate(se3.rotate(p));
}


#endif // RMD_SE3_CUH_

/////////////////////////////////////////////////////////////////////////////////////////////
// Module: AhandaPovRayToStandard
// Author: Emmanuel Trabes
// Copyright: MIT Licence & http://www.doc.ic.ac.uk/~ahanda/ licence.
// Date: 2022, Mar. 30.
// Revision: Rev.1.
// Code reviewer: Andrés Airabella
// Notes: This file converts the file format used at 
// http://www.doc.ic.ac.uk/~ahanda/HighFrameRateTracking/downloads.html
// into the standard [R|T] world -> camera format used by OpenCV
// It is based on a file they provided there, but makes the world coordinate system right handed, 
// with z up, x right, and y forward.
/////////////////////////////////////////////////////////////////////////////////////////////

#ifndef CONVERTAHANDAPOVRAYTOSTANDARD_H_INCLUDED
#define CONVERTAHANDAPOVRAYTOSTANDARD_H_INCLUDED

#include "se3.hpp"

Sophus::SE3f readPose(const char * filename);

#endif 
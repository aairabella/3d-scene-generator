/////////////////////////////////////////////////////////////////////////////////////////////
// Module: TicToc for time measuring.
// Author: Emmanuel Trabes
// Copyright: MIT Licence.
// Date: 2022, Mar. 30.
// Revision: Rev.1.
// Code reviewer: Andrés Airabella
/////////////////////////////////////////////////////////////////////////////////////////////

#ifndef TIC_TOC
#define TIC_TOC

#include <iostream>
#include <ctime>


class tic_toc
{
public:
    struct timespec start_time;

    void tic()
    {
        clock_gettime(CLOCK_MONOTONIC, &start_time);
    }

    double toc()
    {
        struct timespec tv2;

        if (clock_gettime(CLOCK_MONOTONIC, &tv2))
            tv2.tv_sec = tv2.tv_nsec = -1;

        double  sec = static_cast<double>(tv2.tv_sec - start_time.tv_sec);
        double nsec = static_cast<double>(tv2.tv_nsec - start_time.tv_nsec);

        double elapsed= (sec + 1.0e-9 * nsec);

        return elapsed;
    }

private:
};

#endif

#pragma once

#include <opencv2/core.hpp>

#include <iostream>
#include <fstream>

#include <Eigen/Core>
#include "se3.hpp"

#include "Utils/tictoc.h"

#define MAX_LEVELS 6

class PoseEstimatorGradient
{
public:
    PoseEstimatorGradient(float fx, float fy, float cx, float cy, int _width, int _height);

    void setKeyFrame(cv::Mat _keyFrame);
    void setIdepth(cv::Mat _idepth);

    Sophus::SE3f updatePose(cv::Mat frame);

    Sophus::SE3f framePose;

private:

    Eigen::Matrix3f K[MAX_LEVELS];
    Eigen::Matrix3f KInv[MAX_LEVELS];

    int width[MAX_LEVELS], height[MAX_LEVELS];

    cv::Mat keyFrame[MAX_LEVELS];
    cv::Mat idepth[MAX_LEVELS];

    Eigen::Matrix<float, 6, 1> acc_J_pose;
    Eigen::Matrix<float, 6, 6> acc_H_pose;

    float calcResidual(cv::Mat frame, Sophus::SE3f framePose, int lvl);
    void calcHJ(cv::Mat frame, cv::Mat frameDer, Sophus::SE3f framePose, int lvl);
    void calcHJ_2(cv::Mat frame, cv::Mat frameDer, Sophus::SE3f framePose, int lvl);
    void calcHJ_3(cv::Mat frame, cv::Mat frameDer, Sophus::SE3f framePose, int lvl);
    cv::Mat frameDerivative(cv::Mat frame, int lvl);
};

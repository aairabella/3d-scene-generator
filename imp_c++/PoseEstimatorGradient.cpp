#include "PoseEstimatorGradient.h"
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui.hpp>

PoseEstimatorGradient::PoseEstimatorGradient(float fx, float fy, float cx, float cy, int _width, int _height)
{
    for(int lvl = 0; lvl < MAX_LEVELS; lvl++)
    {
        float scale = std::pow(2.0f, float(lvl));

        width[lvl] = int(_width/scale);
        height[lvl] = int(_height/scale);

        K[lvl] = Eigen::Matrix3f::Zero();
        K[lvl](0,0) = fx/scale;
        K[lvl](1,1) = fy/scale;
        K[lvl](2,2) = 1.0f;
        K[lvl](0,2) = cx/scale;
        K[lvl](1,2) = cy/scale;

        KInv[lvl] = K[lvl].inverse();

    }
}

void PoseEstimatorGradient::setKeyFrame(cv::Mat _keyFrame)
{
    for(int lvl = 0; lvl < MAX_LEVELS; lvl++)
    {
        cv::resize(_keyFrame, keyFrame[lvl], cv::Size(width[lvl],height[lvl]),0,0,cv::INTER_AREA);
    }
}

void PoseEstimatorGradient::setIdepth(cv::Mat _idepth)
{
    for(int lvl = 0; lvl < MAX_LEVELS; lvl++)
    {
        cv::resize(_idepth, idepth[lvl], cv::Size(width[lvl],height[lvl]),0,0,cv::INTER_AREA);
    }
}

float PoseEstimatorGradient::calcResidual(cv::Mat frame, Sophus::SE3f framePose, int lvl)
{

    float residual = 0;
    int num = 0;

    for(int y = 0; y < height[lvl]; y++)
        for(int x = 0; x < width[lvl]; x++)
        {
            uchar vkf = keyFrame[lvl].at<uchar>(y,x);
            float keyframeId = idepth[lvl].at<float>(y,x);

            Eigen::Vector3f poinKeyframe = (KInv[lvl]*Eigen::Vector3f(x,y,1.0))/keyframeId;
            Eigen::Vector3f pointFrame = framePose*poinKeyframe;

            if(pointFrame(2) <= 0.0)
                continue;

            Eigen::Vector3f pixelFrame = (K[lvl]*pointFrame)/pointFrame(2);

            if(pixelFrame(0) < 0.0 || pixelFrame(0) > width[lvl] || pixelFrame(1) < 0 || pixelFrame(1) > height[lvl])
                continue;

            uchar vf = frame.at<uchar>(pixelFrame(1), pixelFrame(0));

            float res = (vkf-vf);

            residual += res*res;
            num++;

        }

    return residual/num;
}

Sophus::SE3f PoseEstimatorGradient::updatePose(cv::Mat _frame)
{
    cv::Mat frame[MAX_LEVELS];
    cv::Mat frameDer[MAX_LEVELS];

    for(int lvl = 0; lvl < MAX_LEVELS; lvl++)
    {
        cv::resize(_frame, frame[lvl], cv::Size(width[lvl],height[lvl]),0,0,cv::INTER_AREA);
        frameDer[lvl] = frameDerivative(frame[lvl], lvl);
    }

    int maxIterations[10] = {5, 20, 100, 100, 100, 100, 100, 100, 100, 100};

                    Eigen::Matrix<float, 6, 1> inc;
    for(int lvl=MAX_LEVELS-1; lvl >= 0; lvl--)
    {

        float last_error = calcResidual(frame[lvl],framePose,lvl);

        for(int it = 0; it < maxIterations[lvl]; it++)
        {
            calcHJ_3(frame[lvl], frameDer[lvl], framePose ,lvl);

            float lambda = 0.0;
            int n_try = 0;
            while(true)
            {
                Eigen::Matrix<float, 6, 6> acc_H_pose_lambda;
                acc_H_pose_lambda = acc_H_pose;

                for(int j = 0; j < 6; j++)
                    acc_H_pose_lambda(j,j) *= 1.0 + lambda;

                inc = acc_H_pose_lambda.ldlt().solve(acc_J_pose);

                Sophus::SE3f new_pose = Sophus::SE3f::exp(inc).inverse() * framePose;

                float error = calcResidual(frame[lvl],new_pose,lvl);

                if(error < last_error)
                {

                    framePose = new_pose;

                    float p = error / last_error;


                    if(lambda < 0.2f)
                        lambda = 0.0f;
                    else
                        lambda *= 0.5;

                    last_error = error;

                    if( p >  0.999f)
                    {

                        it = maxIterations[lvl];
                    }

                    //if update accepted, do next iteration
                    break;
                }
                else
                {
                    n_try++;

                    if(lambda < 0.2f)
                        lambda = 0.2f;
                    else
                        lambda *= std::pow(2.0, n_try);

                    //reject update, increase lambda, use un-updated data

                    if(!(inc.dot(inc) > 1e-8))
                    {
                        //if too small, do next level!
                        it = maxIterations[lvl];
                        break;
                    }
                }
            }
        }
    }

    return framePose;
}

void PoseEstimatorGradient::calcHJ(cv::Mat frame, cv::Mat frameDer, Sophus::SE3f framePose, int lvl)
{
    acc_J_pose.setZero();
    acc_H_pose.setZero();

    for(int y = 0; y < height[lvl]; y++)
        for(int x = 0; x < width[lvl]; x++)
        {
            uchar vkf = keyFrame[lvl].at<uchar>(y,x);
            float id = idepth[lvl].at<float>(y,x);

            Eigen::Vector3f rayKeyframe = KInv[lvl]*Eigen::Vector3f(x,y,1.0);
            Eigen::Vector3f poinKeyframe = rayKeyframe/id;
            Eigen::Vector3f pointFrame = framePose*poinKeyframe;

            if(pointFrame(2) <= 0.0)
                continue;

            Eigen::Vector3f rayFrame = pointFrame/pointFrame(2);
            Eigen::Vector3f pixelFrame = K[lvl]*rayFrame;

            if(pixelFrame(0) < 0.0 || pixelFrame(0) >= width[lvl] || pixelFrame(1) < 0.0 || pixelFrame(1) >= height[lvl])
                continue;

            uchar vf = frame.at<uchar>(pixelFrame(1), pixelFrame(0));

            Eigen::Matrix3f d_uf_d_pf;
            d_uf_d_pf(0,0) = K[lvl](0,0);
            d_uf_d_pf(0,1) = 0.0;
            d_uf_d_pf(0,2) = 0.0;

            d_uf_d_pf(1,0) = 0.0;
            d_uf_d_pf(1,1) = K[lvl](1,1);
            d_uf_d_pf(1,2) = 0.0;

            d_uf_d_pf(2,0) = -rayFrame(0)*K[lvl](0,0);
            d_uf_d_pf(2,1) = -rayFrame(1)*K[lvl](1,1);
            d_uf_d_pf(2,2) = 0.0;

            d_uf_d_pf = d_uf_d_pf/pointFrame(2);

            Eigen::Matrix3f d_pf_d_tra;

            d_pf_d_tra(0,0) = 1.0;
            d_pf_d_tra(0,1) = 0.0;
            d_pf_d_tra(0,2) = 0.0;

            d_pf_d_tra(1,0) = 0.0;
            d_pf_d_tra(1,1) = 1.0;
            d_pf_d_tra(1,2) = 0.0;

            d_pf_d_tra(2,0) = 0.0;
            d_pf_d_tra(2,1) = 0.0;
            d_pf_d_tra(2,2) = 1.0;

            Eigen::Matrix3f d_pf_d_rot;
            d_pf_d_rot(0,0) = 0.0;
            d_pf_d_rot(0,1) = -pointFrame(2);
            d_pf_d_rot(0,2) = pointFrame(1);

            d_pf_d_rot(1,0) = pointFrame(2);
            d_pf_d_rot(1,1) = 0.0;
            d_pf_d_rot(1,2) = -pointFrame(0);

            d_pf_d_rot(2,0) = -pointFrame(1);
            d_pf_d_rot(2,1) = pointFrame(0);
            d_pf_d_rot(2,2) = 0.0;

            cv::Vec2f der = frameDer.at<cv::Vec2f>(pixelFrame(1),pixelFrame(0));
            Eigen::Vector2f d_f_d_uf(der.val[0],der.val[1]);

            Eigen::Vector3f d_I_d_pf;
            d_I_d_pf(0) = d_f_d_uf(0)*d_uf_d_pf(0,0);
            d_I_d_pf(1) = d_f_d_uf(1)*d_uf_d_pf(1,1);
            d_I_d_pf(2) = d_f_d_uf(0)*d_uf_d_pf(2,0)+d_f_d_uf(1)*d_uf_d_pf(2,1);

            Eigen::Vector3f d_I_d_rot = d_I_d_pf.transpose()*d_pf_d_rot;
            Eigen::Vector3f d_I_d_tra = d_I_d_pf.transpose()*d_pf_d_tra;

            float residual = (vf - vkf);

            Eigen::Matrix<float, 6, 1> J_pose;
            J_pose << d_I_d_tra(0), d_I_d_tra(1), d_I_d_tra(2), d_I_d_rot(0), d_I_d_rot(1), d_I_d_rot(2);

            acc_J_pose += J_pose*residual;

            for(int i = 0; i < 6; i++)
            {
                for(int j = 0; j < 6; j++)
                {
                    acc_H_pose(i,j) += J_pose[i]*J_pose[j];
                }
            }
        }
}

void PoseEstimatorGradient::calcHJ_2(cv::Mat frame, cv::Mat frameDer, Sophus::SE3f framePose, int lvl)
{
    acc_J_pose.setZero();
    acc_H_pose.setZero();

    for(int y = 0; y < height[lvl]; y++)
        for(int x = 0; x < width[lvl]; x++)
        {
            uchar vkf = keyFrame[lvl].at<uchar>(y,x);
            float keyframeId = idepth[lvl].at<float>(y,x);

            Eigen::Vector3f rayKeyframe = KInv[lvl]*Eigen::Vector3f(x,y,1.0);
            Eigen::Vector3f poinKeyframe = rayKeyframe/keyframeId;
            Eigen::Vector3f pointFrame = framePose*poinKeyframe;

            if(pointFrame(2) <= 0.0)
                continue;

            Eigen::Vector3f rayFrame = pointFrame/pointFrame(2);
            Eigen::Vector3f pixelFrame = K[lvl]*rayFrame;

            if(pixelFrame(0) < 0.0 || pixelFrame(0) >= width[lvl] || pixelFrame(1) < 0.0 || pixelFrame(1) >= height[lvl])
                continue;

            uchar vf = frame.at<uchar>(pixelFrame(1), pixelFrame(0));

            cv::Vec2f der = frameDer.at<cv::Vec2f>(pixelFrame(1),pixelFrame(0));
            Eigen::Vector2f d_f_d_uf(der.val[0],der.val[1]);

            float id = 1.0/pointFrame(2);
            float id2 = 1.0/(pointFrame(2)*pointFrame(2));

            Eigen::Vector2f g = Eigen::Vector2f(d_f_d_uf(0)*K[lvl](0,0), d_f_d_uf(1)*K[lvl](1,1));

            Eigen::Vector3f d_I_d_tra = Eigen::Vector3f(g(0)*id, g(1)*id, -(g(0)*pointFrame(0)*id2 + g(1)*pointFrame(1)*id2));
            Eigen::Vector3f d_I_d_rot = Eigen::Vector3f(-(g(0)*pointFrame(0)*pointFrame(1)*id2 + g(1) + g(1)*pointFrame(1)*pointFrame(1)*id2), g(0) + g(0)*pointFrame(0)*pointFrame(0)*id2 + g(1)*pointFrame(0)*pointFrame(1)*id2, -g(0)*pointFrame(1)*id + g(1)*pointFrame(0)*id);

            float residual = (vf - vkf);

            Eigen::Matrix<float, 6, 1> J_pose;
            J_pose << d_I_d_tra(0), d_I_d_tra(1), d_I_d_tra(2), d_I_d_rot(0), d_I_d_rot(1), d_I_d_rot(2);

            acc_J_pose += J_pose*residual;

            for(int i = 0; i < 6; i++)
            {
                for(int j = 0; j < 6; j++)
                {
                    acc_H_pose(i,j) += J_pose[i]*J_pose[j];
                }
            }
        }
}

void PoseEstimatorGradient::calcHJ_3(cv::Mat frame, cv::Mat frameDer, Sophus::SE3f framePose, int lvl)
{
    acc_J_pose.setZero();
    acc_H_pose.setZero();

    for(int y = 0; y < height[lvl]; y++)
        for(int x = 0; x < width[lvl]; x++)
        {
            uchar vkf = keyFrame[lvl].at<uchar>(y,x);
            float keyframeId = idepth[lvl].at<float>(y,x);
            Eigen::Vector3f poinKeyframe = (KInv[lvl]*Eigen::Vector3f(x,y,1.0))/keyframeId;
            Eigen::Vector3f pointFrame = framePose*poinKeyframe;
            if(pointFrame(2) <= 0.0)
                continue;
            Eigen::Vector3f pixelFrame = (K[lvl]*pointFrame)/pointFrame(2);
            if(pixelFrame(0) < 0.0 || pixelFrame(0) >= width[lvl] || pixelFrame(1) < 0.0 || pixelFrame(1) >= height[lvl])
                continue;
            uchar vf = frame.at<uchar>(pixelFrame(1), pixelFrame(0));

            cv::Vec2f der = frameDer.at<cv::Vec2f>(pixelFrame(1),pixelFrame(0));
            Eigen::Vector2f d_f_d_uf(der.val[0],der.val[1]);
            float id = 1.0/pointFrame(2);

            float v0 = d_f_d_uf(0) * K[lvl](0,0) * id;
            float v1 = d_f_d_uf(1) * K[lvl](1,1) * id;
            float v2 = -(v0 * pointFrame(0) + v1 * pointFrame(1)) * id;
            Eigen::Vector3f d_I_d_tra = Eigen::Vector3f(v0, v1, v2);
            Eigen::Vector3f d_I_d_rot = Eigen::Vector3f( -pointFrame(2) * v1 + pointFrame(1) * v2, pointFrame(2) * v0 - pointFrame(0) * v2, -pointFrame(1) * v0 + pointFrame(0) * v1);

            float residual = (vf - vkf);

            Eigen::Matrix<float, 6, 1> J_pose;
            J_pose << d_I_d_tra(0), d_I_d_tra(1), d_I_d_tra(2), d_I_d_rot(0), d_I_d_rot(1), d_I_d_rot(2);

            acc_J_pose += J_pose*residual;
            for(int i = 0; i < 6; i++)
            {
                for(int j = 0; j < 6; j++)
                {
                    acc_H_pose(i,j) += J_pose[i]*J_pose[j];
                }
            }
        }
}

cv::Mat PoseEstimatorGradient::frameDerivative(cv::Mat frame, int lvl)
{
    cv::Mat der(height[lvl], width[lvl], CV_32FC2, cv::Vec2f(0.0,0.0));

    for(int y = 1; y < height[lvl]-1; y++)
        for(int x = 1; x < width[lvl]-1; x++)
        {
            cv::Vec2f d;
            d.val[0] = (frame.at<uchar>(y,x+1) - frame.at<uchar>(y,x-1))/2.0;
            d.val[1] = (frame.at<uchar>(y+1,x) - frame.at<uchar>(y-1,x))/2.0;

            der.at<cv::Vec2f>(y,x) = d;
        }

    return der;
}

﻿#pragma once
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>

#include <opencv2/ximgproc/edge_filter.hpp>

#define costVolumeLayers 128

class CostVolume{
public:

    CostVolume(int height, int width){

        for(int z = 0; z < costVolumeLayers; z++)
        {
            cost[z] = cv::Mat_<float>(height, width, 255.0);
            info[z] = cv::Mat_<float>(height, width, 0.0);
        }

        reset();
    }

    ~CostVolume(){

    }

    void reset()
    {
        for(int z = 0; z < costVolumeLayers; z++)
        {

            cost[z].setTo(255.0);
            info[z].setTo(0.0);
        }
    }

    inline float get(int y, int x, int z)
    {
       return cost[z](y,x);
    }
    /*
    inline void set(int y, int x, int z, float cost)
    {
        cost[z](y,x) = cost;
    }
    */

    inline void update(int y, int x, int z, float newCost, float validity)
    {
        float info_sum = info[z](y,x) + validity;
        float p = validity/info_sum;
        cost[z](y,x) = cost[z](y,x) + (newCost - cost[z](y,x))*p;
        info[z](y,x) = info_sum;
    }

    void filter(cv::Mat_<float> &guide)
    {
//        int win_size = 9;
//        float eps = 0.3*0.3;
//        int win_size = 4;
//        float eps = 0.1*0.1;
        int radius = 4;
        float eps = 0.4*0.4;

        cv::Ptr<cv::ximgproc::GuidedFilter> g_filter = cv::ximgproc::createGuidedFilter(guide, radius, eps);

        for(int z = 0; z < costVolumeLayers; z++)
        {
            g_filter->filter(cost[z], cost[z]);
            //cv::blur(cost[z], cost[z], cv::Size(kernel_size,kernel_size));
        }
    }
/*
    cv::Mat_<float> getDebugImage()
    {
        int height = height/2;
        cv::Mat_<float> debugImage(costVolumeLayers, width, 0.0);

        for(int z = 0; z < costVolumeLayers; z++)
        {
            for(int x = 0; x < costVolumeWidth; x++)
            {
                debugImage(z,x) = cost[z](height,x);
            }
        }

        return debugImage;
    }
    */

    inline float getCost(int y, int x, int z)
    {
        return cost[z](y,x);
    }

    inline float getInfo(int y, int x, int z)
    {
        return info[z](y,x);
    }


private:

    cv::Mat_<float> cost[costVolumeLayers];
    cv::Mat_<float> info[costVolumeLayers];
};

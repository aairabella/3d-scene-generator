/////////////////////////////////////////////////////////////////////////////////////////////
// Module: Main CPU implementation of pose estimator.
// Author: Emmanuel Trabes
// Copyright: MIT Licence.
// Date: 2022, Mar. 30.
// Revision: Rev.1.
// Code reviewer: Andrés Airabella
/////////////////////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include <stdio.h>
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>

#include "utils/convertAhandaPovRayToStandard.h"
#include "PoseEstimatorGradient.h"
#include <Eigen/Core>
#include "se3.hpp"
#include "Utils/tictoc.h"

inline bool fileExist(const std::string& name) {
    std::ifstream f(name.c_str());
    return f.good();
}

int main(void)
{
    bool printResults = false;
    tic_toc t;
    int frameNumber = 0;
    int frameCounterDirection = 1;

    int framesToTrack = 599;
    int framesTracked = 0;

    int width = 640;
    int height = 480;

    float fx, fy, cx, cy;
    fx = 481.20; fy = 480.0; cx = 319.5; cy = 239.5;

    cv::Mat keyFrame = cv::imread("../desktop_dataset/scene_000.png", cv::IMREAD_GRAYSCALE);
    Sophus::SE3f keyframePose = readPose("../desktop_dataset/scene_000.txt");

    cv::Mat iDepth;
    cv::FileStorage fs("../desktop_dataset/scene_depth_000.yml", cv::FileStorage::READ );
    fs["idepth"] >> iDepth;

    PoseEstimatorGradient poseEstimator(fx,fy,cx,cy,width,height);

    poseEstimator.setKeyFrame(keyFrame.clone());
    poseEstimator.setIdepth(iDepth);


    while(true){
        framesTracked++;
        frameNumber += frameCounterDirection;
        if(frameNumber > 598)
            frameCounterDirection = -1;
        if(frameNumber < 2)
            frameCounterDirection = 1;

        char image_filename[500];
        char RT_filename[500];

        sprintf(image_filename,"../desktop_dataset/scene_%03d.png", frameNumber);
        sprintf(RT_filename,"../desktop_dataset/scene_%03d.txt", frameNumber);

        Sophus::SE3f pose = readPose(RT_filename);
        cv::Mat frame = cv::imread(image_filename, cv::IMREAD_GRAYSCALE);

        Sophus::SE3f realPose = pose*keyframePose.inverse();
        t.tic();
        poseEstimator.updatePose(frame);
        std::cout << "time updatePose " << framesTracked << " " << t.toc() << std::endl;
        if (printResults){
            std::cout << "real pose " << std::endl;
            std::cout << realPose.matrix() << std::endl;
            std::cout << "est pose " << std::endl;
            std::cout << poseEstimator.framePose.matrix() << std::endl << std::endl;
        }
        cv::imshow("image", frame);
        cv::imshow("keyframe", keyFrame);
        cv::imshow("idepth", iDepth);
        cv::waitKey(30);

        if(framesTracked >= framesToTrack)
        {
            return 0;
        }
        std::cout << framesTracked << std::endl;
    }

    return 1;
}
